# Copyright (C) 2016 Li Cheng at Beijing University of Posts
# and Telecommunications. www.muzixing.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# conding=utf-8
import json, time #BOOTSCAT ADD
import numpy as np #BOOTSCAT ADD
from webob import Response #BOOTSCAT ADD
from ryu.app.wsgi import ControllerBase, WSGIApplication, route #BOOTSCAT ADD
import logging
import struct
import networkx as nx
from operator import attrgetter
from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import MAIN_DISPATCHER, DEAD_DISPATCHER
from ryu.controller.handler import CONFIG_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib import hub
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ipv4
from ryu.lib.packet import arp

from ryu.topology import event, switches
from ryu.topology.api import get_switch, get_link

import network_awareness
import network_monitor
from pprint import pprint
# from ryu.topology.tracker import Tracker

global_sla = {}
global_paths = {} #(src_ip, dst_ip): [path, flow_info]
packetin_lst = []

class Shortest_Forwarding(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]
    _CONTEXTS = {
        "network_awareness": network_awareness.NetworkAwareness,
        "network_monitor": network_monitor.NetworkMonitor,
        "wsgi": WSGIApplication} #BOOTSCAT ADD

    def __init__(self, *args, **kwargs):
        super(Shortest_Forwarding, self).__init__(*args, **kwargs)
        wsgi = kwargs['wsgi'] #BOOTSCAT ADD
        wsgi.register(TopologyController, {'topology_api_app': self}) #BOOTSCAT ADD
        """EVALUATION"""
        self.count = 0
        self.time = time.time()
        # self.per_flow = []
        """"""
        self.name = 'shortest_forwarding'
        self.awareness = kwargs["network_awareness"]
        self.monitor = kwargs["network_monitor"]
        self.mac_to_port = {}
        self.datapaths = {}
        self.reroute_thread = hub.spawn(self._reroute) #BOOTSCAT ADD

    @set_ev_cls(ofp_event.EventOFPStateChange,
                [MAIN_DISPATCHER, DEAD_DISPATCHER])
    def _state_change_handler(self, ev):
        datapath = ev.datapath
        if ev.state == MAIN_DISPATCHER:
            if not datapath.id in self.datapaths:
                self.logger.debug('register datapath: %016x', datapath.id)
                self.datapaths[datapath.id] = datapath
        elif ev.state == DEAD_DISPATCHER:
            if datapath.id in self.datapaths:
                self.logger.debug('unregister datapath: %016x', datapath.id)
                del self.datapaths[datapath.id]

    def add_flow(self, dp, p, match, actions, idle_timeout=0, hard_timeout=0):
        ofproto = dp.ofproto
        parser = dp.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]

        mod = parser.OFPFlowMod(datapath=dp, priority=p,
                                idle_timeout=idle_timeout,
                                hard_timeout=hard_timeout,
                                match=match, instructions=inst)
        dp.send_msg(mod)

    def send_flow_mod(self, datapath, flow_info, src_port, dst_port):
        parser = datapath.ofproto_parser
        actions = []
        actions.append(parser.OFPActionOutput(dst_port))

        match = parser.OFPMatch(
            in_port=src_port, eth_type=flow_info[0],
            ipv4_src=flow_info[1], ipv4_dst=flow_info[2])
        self.add_flow(datapath, 1, match, actions,
                      idle_timeout=15, hard_timeout=60)

    def _build_packet_out(self, datapath, buffer_id, src_port, dst_port, data):
        actions = []
        if dst_port:
            actions.append(datapath.ofproto_parser.OFPActionOutput(dst_port))

        msg_data = None
        if buffer_id == datapath.ofproto.OFP_NO_BUFFER:
            if data is None:
                return None
            msg_data = data

        out = datapath.ofproto_parser.OFPPacketOut(
            datapath=datapath, buffer_id=buffer_id,
            data=msg_data, in_port=src_port, actions=actions)
        return out

    def send_packet_out(self, datapath, buffer_id, src_port, dst_port, data):
        out = self._build_packet_out(datapath, buffer_id,
                                     src_port, dst_port, data)
        if out:
            datapath.send_msg(out)

    def get_port(self, dst_ip, access_table):
        # access_table: {(sw,port) :(ip, mac)}
        if access_table:
            if isinstance(access_table.values()[0], tuple):
                for key in access_table.keys():
                    if dst_ip == access_table[key][0]:
                        dst_port = key[1]
                        return dst_port
        return None

    def get_link_to_port(self, link_to_port, src_dpid, dst_dpid):
        if (src_dpid, dst_dpid) in link_to_port:
            return link_to_port[(src_dpid, dst_dpid)]
        else:
            self.logger.info("dpid:%s->dpid:%s is not in links" % (
                             src_dpid, dst_dpid))
            return None

    def flood(self, msg):
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        for dpid in self.awareness.access_ports:
            for port in self.awareness.access_ports[dpid]:
                if (dpid, port) not in self.awareness.access_table.keys():
                    datapath = self.datapaths[dpid]
                    out = self._build_packet_out(
                        datapath, ofproto.OFP_NO_BUFFER,
                        ofproto.OFPP_CONTROLLER, port, msg.data)
                    datapath.send_msg(out)
        self.logger.debug("Flooding msg")

    def arp_forwarding(self, msg, src_ip, dst_ip):
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        result = self.awareness.get_host_location(dst_ip)
        if result:  # host record in access table.
            datapath_dst, out_port = result[0], result[1]
            datapath = self.datapaths[datapath_dst]
            out = self._build_packet_out(datapath, ofproto.OFP_NO_BUFFER,
                                         ofproto.OFPP_CONTROLLER,
                                         out_port, msg.data)
            datapath.send_msg(out)
            self.logger.debug("Reply ARP to knew host")
        else:
            self.flood(msg)

    def get_path(self, src, dst):
        return self.awareness.shortest_paths.get(src).get(dst)

    def get_sw(self, dpid, in_port, src, dst):
        src_sw = dpid
        dst_sw = None

        src_location = self.awareness.get_host_location(src)
        if in_port in self.awareness.access_ports[dpid]:
            if (dpid,  in_port) == src_location:
                src_sw = src_location[0]
            else:
                return None

        dst_location = self.awareness.get_host_location(dst)
        if dst_location:
            dst_sw = dst_location[0]

        return src_sw, dst_sw

    def install_flow(self, datapaths, link_to_port, access_table, path,
                     flow_info, buffer_id, data=None, packet=True): #BOOTSCAT MOD
        ''' path=[dpid1, dpid2...]
            flow_info=(eth_type, src_ip, dst_ip, in_port)
        '''
        if path is None or len(path) == 0:
            self.logger.info("Path error!")
            return
        in_port = flow_info[3]
        first_dp = datapaths[path[0]]
        out_port = first_dp.ofproto.OFPP_LOCAL
        back_info = (flow_info[0], flow_info[2], flow_info[1])
        # inter_link
        if len(path) > 2:
            for i in xrange(1, len(path)-1):
                port = self.get_link_to_port(link_to_port, path[i-1], path[i])
                port_next = self.get_link_to_port(link_to_port,
                                                  path[i], path[i+1])
                if port and port_next:
                    src_port, dst_port = port[1], port_next[0]
                    datapath = datapaths[path[i]]
                    self.send_flow_mod(datapath, flow_info, src_port, dst_port)
                    self.send_flow_mod(datapath, back_info, dst_port, src_port)
                    self.logger.debug("inter_link flow install")
        if len(path) > 1:
            # the last flow entry: tor -> host
            port_pair = self.get_link_to_port(link_to_port, path[-2], path[-1])
            if port_pair is None:
                self.logger.info("Port is not found")
                return
            src_port = port_pair[1]

            dst_port = self.get_port(flow_info[2], access_table)
            if dst_port is None:
                self.logger.info("Last port is not found.")
                return

            last_dp = datapaths[path[-1]]
            self.send_flow_mod(last_dp, flow_info, src_port, dst_port)
            self.send_flow_mod(last_dp, back_info, dst_port, src_port)

            # the first flow entry
            port_pair = self.get_link_to_port(link_to_port, path[0], path[1])
            if port_pair is None:
                self.logger.info("Port not found in first hop.")
                return
            out_port = port_pair[0]
            self.send_flow_mod(first_dp, flow_info, in_port, out_port)
            self.send_flow_mod(first_dp, back_info, out_port, in_port)
            if packet:
                self.send_packet_out(first_dp, buffer_id, in_port, out_port, data)

        # src and dst on the same datapath
        else:
            out_port = self.get_port(flow_info[2], access_table)
            if out_port is None:
                self.logger.info("Out_port is None in same dp")
                return
            self.send_flow_mod(first_dp, flow_info, in_port, out_port)
            self.send_flow_mod(first_dp, back_info, out_port, in_port)
            if packet:
                self.send_packet_out(first_dp, buffer_id, in_port, out_port, data)

    """Main modify part"""
    def get_qos_path(self, src_sw, dst_sw, qos_ip):
        constraints = global_sla[qos_ip]
        # self.awareness.get_link_state(monitorData=self.monitor) #BOOTSCAT
        qos_paths = self.awareness.get_qos_paths(cutoff=None, weight='weight',
                                                 constraints=constraints)
        # print qos_paths.get(src_sw).get(dst_sw)
        return qos_paths.get(src_sw).get(dst_sw)

    def shortest_forwarding(self, msg, eth_type, ip_src, ip_dst):
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        result = self.get_sw(datapath.id, in_port, ip_src, ip_dst)
        if result:
            src_sw, dst_sw = result[0], result[1]
            if dst_sw:

                flow_info = (eth_type, ip_src, ip_dst, in_port)
                """ericdeng"""
                if ip_src in global_sla.keys():
                    path = self.get_qos_path(src_sw, dst_sw, ip_src)
                    # print "INFO: forwarding.py finds qos path"
                    # print ip_src, "-->",
                    # for sw in path:
                    #     print sw, "-->",
                    # print ip_dst
                    # print "==================================\n"
                else:
                    path = self.get_path(src_sw, dst_sw)
                    # print "INFO: forwarding.py finds shortest path"
                    # print ip_src, "-->",
                    # for sw in path:
                    #     print sw, "-->",
                    # print ip_dst
                    # print "==================================\n"
                # set global_paths
                global_paths[(ip_src, ip_dst)] = [list(path), flow_info]
                """"""
                # self.logger.info("[PATH]%s<-->%s: %s" % (ip_src, ip_dst, path))
                if src_sw == 3004:
                    if 3004 in path:
                        path.remove(3004)
                self.install_flow(self.datapaths,
                                  self.awareness.link_to_port,
                                  self.awareness.access_table, path,
                                  flow_info, msg.buffer_id, msg.data)
        return

    # BOOTSCAT ADD
    def _reroute(self):
        """check reroute when src ip have SLA automatically every 5s,if have new qos path then
        remove flow rule of old path and install flow rule of new path
        """
        while True:
            print "***********************************"
            timeStamp = time.time()
            count = self.count
            print "INFO: %f packetIn/s" % (count / (timeStamp - self.time))
            self.count = 0
            self.time = time.time()
            print "INFO: forwarding.py reroute monitor."
            pprint(global_paths)
            print "===================================\n"
            self.awareness.get_link_state(monitorData=self.monitor) #BOOTSCAT
            for key in global_paths:
                src_ip = key[0]
                dst_ip = key[1]
                if src_ip in global_sla:
                    old_path = global_paths[key][0]
                    # print "***old_path: ", old_path
                    flow_info = global_paths[key][1]
                    new_path = self.get_qos_path(old_path[0], old_path[-1], src_ip)
                    # print "***new_path: ", new_path
                    if old_path != new_path:
                        print "INFO: forwarding.py removes flow."
                        self.remove_flow(self.datapaths, self.awareness.link_to_port, self.awareness.access_table, old_path, flow_info)
                        print "INFO: forwarding.py rerouting."
                        print src_ip, "-->",
                        for sw in new_path:
                            print sw, "-->",
                        print dst_ip
                        print "==============================\n"
                        self.install_flow(self.datapaths,
                                          self.awareness.link_to_port,
                                          self.awareness.access_table, new_path,
                                          flow_info, None, None, False)
                        # update new path to global_paths
                        global_paths[(src_ip, dst_ip)] = [list(new_path), flow_info]
            hub.sleep(5)

    def remove_flow(self, datapaths, link_to_port, access_table, path,
                     flow_info): #BOOTSCAT MOD
        ''' path=[dpid1, dpid2...]
            flow_info=(eth_type, src_ip, dst_ip, in_port)
        '''
        if path is None or len(path) == 0:
            self.logger.info("Path error!")
            return
        in_port = flow_info[3]
        first_dp = datapaths[path[0]]
        out_port = first_dp.ofproto.OFPP_LOCAL
        back_info = (flow_info[0], flow_info[2], flow_info[1])
        # inter_link
        if len(path) > 2:
            for i in xrange(1, len(path)-1):
                port = self.get_link_to_port(link_to_port, path[i-1], path[i])
                port_next = self.get_link_to_port(link_to_port,
                                                  path[i], path[i+1])
                if port and port_next:
                    src_port, dst_port = port[1], port_next[0]
                    datapath = datapaths[path[i]]
                    self.send_flow_del(datapath, flow_info, src_port, dst_port)
                    self.send_flow_del(datapath, back_info, dst_port, src_port)
        if len(path) > 1:
            # the last flow entry: tor -> host
            port_pair = self.get_link_to_port(link_to_port, path[-2], path[-1])
            if port_pair is None:
                return
            src_port = port_pair[1]

            dst_port = self.get_port(flow_info[2], access_table)
            if dst_port is None:
                return

            last_dp = datapaths[path[-1]]
            self.send_flow_del(last_dp, flow_info, src_port, dst_port)
            self.send_flow_del(last_dp, back_info, dst_port, src_port)

            # the first flow entry
            port_pair = self.get_link_to_port(link_to_port, path[0], path[1])
            if port_pair is None:
                return
            out_port = port_pair[0]
            self.send_flow_del(first_dp, flow_info, in_port, out_port)
            self.send_flow_del(first_dp, back_info, out_port, in_port)

        # src and dst on the same datapath
        else:
            out_port = self.get_port(flow_info[2], access_table)
            if out_port is None:
                return
            self.send_flow_del(first_dp, flow_info, in_port, out_port)
            self.send_flow_del(first_dp, back_info, out_port, in_port)

    def send_flow_del(self, datapath, flow_info, src_port, dst_port):
        parser = datapath.ofproto_parser
        ofproto = datapath.ofproto

        match = parser.OFPMatch(
            in_port=src_port, eth_type=flow_info[0],
            ipv4_src=flow_info[1], ipv4_dst=flow_info[2])

        mod = parser.OFPFlowMod(
            datapath, command=ofproto.OFPFC_DELETE,
            out_port=ofproto.OFPP_ANY, out_group=ofproto.OFPG_ANY,
            priority=1, match=match)

        datapath.send_msg(mod)
    """/modify"""

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        '''
            In packet_in handler, we need to learn access_table by ARP.
            Therefore, the first packet from UNKOWN host MUST be ARP.
        '''
        # """EVALUATION"""
        # a = time.time()
        self.count += 1
        # """"""
        msg = ev.msg
        datapath = msg.datapath
        in_port = msg.match['in_port']
        pkt = packet.Packet(msg.data)
        arp_pkt = pkt.get_protocol(arp.arp)
        ip_pkt = pkt.get_protocol(ipv4.ipv4)

        if isinstance(arp_pkt, arp.arp):
            self.logger.debug("ARP processing")
            self.arp_forwarding(msg, arp_pkt.src_ip, arp_pkt.dst_ip)

        if isinstance(ip_pkt, ipv4.ipv4):
            self.logger.debug("IPV4 processing")
            if len(pkt.get_protocols(ethernet.ethernet)):
                eth_type = pkt.get_protocols(ethernet.ethernet)[0].ethertype
                self.shortest_forwarding(msg, eth_type, ip_pkt.src, ip_pkt.dst)
                # """EVALUATION"""
                # b = time.time()
                # self.per_flow.append((b-a))
                # """"""

        # """EVALUATION"""
        # if len(self.per_flow) == 10000:
        #     print "packet in handle time"
        #     print np.mean(self.per_flow)
        # """"""
    ################################################################BOOTSCAT ADD
class TopologyController(ControllerBase):
    def __init__(self, req, link, data, **config):
        super(TopologyController, self).__init__(req, link, data, **config)
        self.topology_api_app = data['topology_api_app']

    @route('SLA', '/SLA/regist/{src}/{bw}/{plr}/{d}', methods=['GET'])
    def regist_sla(self, req, **kwargs):
        tmp_dict = {'bw': kwargs['bw'], 'plr': kwargs['plr'], 'd': kwargs['d']}
        global_sla[kwargs['src']] = tmp_dict
        print "INFO: Set service level agreement"
        print "=>", kwargs
        result = json.dumps(global_sla)
        return Response(content_type='application/json', body=result)

    ################################################################
