# Copyright (C) 2016 Li Cheng at Beijing University of Posts
# and Telecommunications. www.muzixing.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# conding=utf-8
import logging
import struct
import copy
import networkx as nx
from operator import attrgetter
from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import MAIN_DISPATCHER, DEAD_DISPATCHER
from ryu.controller.handler import CONFIG_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ipv4
from ryu.lib.packet import arp
from ryu.lib import hub

from ryu.topology import event, switches
from ryu.topology.api import get_switch, get_link

from pprint import pprint


class NetworkAwareness(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]
    SLEEP_PERIOD = 10
    IS_UPDATE = True

    def __init__(self, *args, **kwargs):
        super(NetworkAwareness, self).__init__(*args, **kwargs)
        self.topology_api_app = self
        self.name = "awareness"
        self.link_to_port = {}       # (src_dpid,dst_dpid)->(src_port,dst_port)
        self.link_state = {}         # (src_dpid,dst_dpid)->{'bw': num, 'plr': num, 'd': num} #BOOTSCAT ADD
        self.access_table = {}       # {(sw,port) :[host1_ip]}
        self.switch_port_table = {}  # dpip->port_num
        self.access_ports = {}       # dpid->port_num
        self.interior_ports = {}     # dpid->port_num

        self.graph = nx.DiGraph()
        self.weight_graph = nx.DiGraph() #BOOTSCAT MOD
        self.pre_graph = nx.DiGraph()
        self.pre_access_table = {}
        self.pre_link_to_port = {}
        self.shortest_paths = None
        self.qos_paths = None # ericdeng add

        self.discover_thread = hub.spawn(self._discover)

    def _discover(self):
        i = 0
        while True:
            self.show_topology()
            if i == 5:
                self.get_topology(None)
                i = 0
            hub.sleep(self.SLEEP_PERIOD)
            i = i + 1

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        msg = ev.msg
        self.logger.info("switch:%s connected", datapath.id)

        # install table-miss flow entry
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions)
        # if datapath.id == 3004:
        #     print "I'm 3004"
        #     self.add_flow(datapath, 2, match, actions)

    def add_flow(self, dp, p, match, actions, idle_timeout=0, hard_timeout=0):
        ofproto = dp.ofproto
        parser = dp.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]

        mod = parser.OFPFlowMod(datapath=dp, priority=p,
                                idle_timeout=idle_timeout,
                                hard_timeout=hard_timeout,
                                match=match, instructions=inst)
        dp.send_msg(mod)

    def get_host_location(self, host_ip):
        for key in self.access_table.keys():
            if self.access_table[key][0] == host_ip:
                return key
        # self.logger.info("%s location is not found." % host_ip) #ericdeng
        return None

    def get_switches(self):
        return self.switches

    def get_links(self):
        return self.link_to_port

    # get Adjacency matrix from link_to_port
    def get_graph(self, link_list):
        for src in self.switches:
            for dst in self.switches:
                self.graph.add_edge(src, dst, weight=float('inf'))
                if src == dst:
                    self.graph.add_edge(src, dst, weight=0)
                elif (src, dst) in link_list:
                    self.graph.add_edge(src, dst, weight=1)

        return self.graph

    def get_link_state(self, monitorData):
        """return dict
            (src_dpid,dst_dpid)->{'bw': num, 'plr': num, 'd': num} #BOOTSCAT ADD
        """
        """BOOTSCAT"""
        # while True:
        bodys = monitorData.stats['port']
            # if bodys != {}:
                # break
        for key in self.link_to_port:
            (src_dpid, dst_dpid) = key
            reverse = (dst_dpid, src_dpid)
            (src_port, dst_port) = self.link_to_port[key]
            try:
                bandwidth = self.get_bw(monitorData, src_dpid, src_port, dst_dpid, dst_port)
            except Exception:
                # print "bandwidth computing..."
                bandwidth = 0
                pass
            try:
                packet_loss_rate = self.get_plr(bodys[src_dpid], bodys[dst_dpid], src_port, dst_port)
            except Exception:
                # print "packet_loss_rate computing..."
                packet_loss_rate = 0
                pass
            self.link_state[key] = {"bw": bandwidth, "plr": packet_loss_rate, "d": 0}
            self.link_state[reverse] = {"bw": bandwidth, "plr": packet_loss_rate, "d": 0}
        # print "INFO: awareness.py print link state"
        # for key in self.link_state:
        #     print key, ": ", self.link_state[key]
        # print "===================================\n"
        """"""

    """"""
    def get_bw(self, monitorData, src_dpid, src_port, dst_dpid, dst_port):
        src_b = monitorData.capacity - monitorData.port_speed[(src_dpid, src_port)][-1] * 8 / 1000000.0
        dst_b = monitorData.capacity - monitorData.port_speed[(dst_dpid, dst_port)][-1] * 8 / 1000000.0
        bw = min(src_b, dst_b)
        return bw

    def get_plr(self, src_stats, dst_stats, src_port, dst_port):
        for stat in src_stats:
            if stat.port_no == src_port:
                src_tx_packets = stat.tx_packets
                src_rx_packets = stat.rx_packets
                break
        for stat in dst_stats:
            if stat.port_no == dst_port:
                dst_tx_packets = stat.tx_packets
                dst_rx_packets = stat.rx_packets
                break
        plr = float(src_tx_packets - dst_rx_packets)/src_tx_packets
        return plr
        # return 1.0 - (dst_rx_packets + src_rx_packets) / float(dst_tx_packets + src_tx_packets)

    ###################################################################### BOOTSCAT ADD
    def get_weight_graph(self, link_list, constraints):
        graph = {}
        for src in self.switches:
            for dst in self.switches:
                self.weight_graph.add_edge(src, dst, weight=float('inf'))
                graph[(src, dst)] = "x"
                if src == dst:
                    self.weight_graph.add_edge(src, dst, weight=0)
                    graph[(src, dst)] = 0
                elif (src, dst) in link_list:
                    # constraints = {'bw': 100, 'plr': 2, 'd': 1}
                    # link weight computation
                    weight = self.get_link_weight( (src, dst), constraints)
                    self.weight_graph.add_edge(src, dst, weight=weight)
                    graph[(src, dst)] = weight
        print "INFO: awareness.py print weight graph"
        print "    |",
        for src in self.switches:
            print "%5s" % (str(src)),
        print "\n-----------------------------------"
        for src in self.switches:
            print "%-4s|" % (str(src)),
            for dst in self.switches:
                print "%5s" % (str(graph[(src, dst)])),
            print ""
        print "=====================================\n"
        return self.weight_graph

    def get_link_weight(self, link, constraints):
        cost = self.link_state[link]['d'] * 100
        for key in constraints:
            link_state = self.link_state[link][key]
            requirement = float(constraints[key])
            if requirement == -1:
                continue

            if key == 'bw':
                upper_bound = requirement * 2
                if link_state < requirement:
                    cost += ((link_state - requirement) / requirement * 100) ** 2
                elif link_state <= upper_bound:
                    cost += max((requirement - link_state) / requirement, -1 * cost)

            elif key == 'plr':
                if link_state > requirement:
                    cost += ((requirement - link_state) / requirement * 100) ** 2

            elif key == 'd':
                if link_state > requirement:
                    cost += ((requirement - link_state) / link_state * 100) ** 2

        return max(cost, 1)

    ######################################################################

    def create_port_map(self, switch_list):
        for sw in switch_list:
            dpid = sw.dp.id
            self.switch_port_table.setdefault(dpid, set())
            self.interior_ports.setdefault(dpid, set())
            self.access_ports.setdefault(dpid, set())

            for p in sw.ports:
                self.switch_port_table[dpid].add(p.port_no)

    # get links`srouce port to dst port  from link_list,
    # link_to_port:(src_dpid,dst_dpid)->(src_port,dst_port)
    def create_interior_links(self, link_list):
        for link in link_list:
            src = link.src
            dst = link.dst
            self.link_to_port[
                (src.dpid, dst.dpid)] = (src.port_no, dst.port_no)

            # find the access ports and interiorior ports
            if link.src.dpid in self.switches:
                self.interior_ports[link.src.dpid].add(link.src.port_no)
            if link.dst.dpid in self.switches:
                self.interior_ports[link.dst.dpid].add(link.dst.port_no)

    # get ports without link into access_ports
    def create_access_ports(self):
        for sw in self.switch_port_table:
            self.access_ports[sw] = self.switch_port_table[
                sw] - self.interior_ports[sw]

    def floyd_dict(self, graph, cutoff=None, weight='weight'):
        return nx.all_pairs_dijkstra_path(graph, cutoff=cutoff, weight=weight)

    def get_shortest_paths(self, cutoff=None, weight='weight'): #BOOTSCAT MOD
        self.shortest_paths = self.floyd_dict(self.graph, cutoff=cutoff,
                                              weight=weight)
        return self.shortest_paths

    ###############################################################BOOTSCAT ADD
    def get_qos_paths(self, cutoff=None, weight='weight', constraints={}):
        self.get_weight_graph(self.link_to_port.keys(), constraints)
        self.qos_paths = self.floyd_dict(self.weight_graph, cutoff=cutoff,
                                              weight=weight)
        return self.qos_paths
    ###############################################################

    events = [event.EventSwitchEnter,
              event.EventSwitchLeave, event.EventPortAdd,
              event.EventPortDelete, event.EventPortModify,
              event.EventLinkAdd, event.EventLinkDelete]

    @set_ev_cls(events)
    def get_topology(self, ev):
        switch_list = get_switch(self.topology_api_app, None)
        self.create_port_map(switch_list)
        self.switches = self.switch_port_table.keys()
        links = get_link(self.topology_api_app, None)
        self.create_interior_links(links)
        self.create_access_ports()
        self.get_graph(self.link_to_port.keys())
        self.get_shortest_paths(weight='weight')

    def register_access_info(self, dpid, in_port, ip, mac):
        if in_port in self.access_ports[dpid]:
            if (dpid, in_port) in self.access_table:
                if self.access_table[(dpid, in_port)] == (ip, mac):
                    return
                else:
                    self.access_table[(dpid, in_port)] = (ip, mac)
                    return
            else:
                self.access_table.setdefault((dpid, in_port), None)
                self.access_table[(dpid, in_port)] = (ip, mac)
                return

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        msg = ev.msg
        datapath = msg.datapath

        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']
        pkt = packet.Packet(msg.data)

        eth_type = pkt.get_protocols(ethernet.ethernet)[0].ethertype
        arp_pkt = pkt.get_protocol(arp.arp)
        ip_pkt = pkt.get_protocol(ipv4.ipv4)

        if arp_pkt:
            arp_src_ip = arp_pkt.src_ip
            arp_dst_ip = arp_pkt.dst_ip
            mac = arp_pkt.src_mac

            # record the access info
            self.register_access_info(datapath.id, in_port, arp_src_ip, mac)

    def show_topology(self):
        switch_num = len(self.graph.nodes())
        if self.pre_graph != self.graph or self.IS_UPDATE:
            """Delete"""
            self.pre_graph = copy.deepcopy(self.graph)

        if self.pre_link_to_port != self.link_to_port or self.IS_UPDATE:
            """Delete"""
            self.pre_link_to_port = copy.deepcopy(self.link_to_port)

        if self.pre_access_table != self.access_table or self.IS_UPDATE:
            self.pre_access_table = copy.deepcopy(self.access_table)
