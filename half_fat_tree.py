#!/usr/bin/python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.link import TCLink
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel
from mininet.node import Controller, RemoteController

class HalfFatTreeTopo(Topo):
    # "Create Fat Tree Topology"

    # every nodes list
    coreSwitch = []
    aggrSwitch = []
    edgeSwitch = []
    host = []

    def __init__(self):
        coreSwitchNumber = 2
        self.coreSwitchNumber = coreSwitchNumber
        self.aggrSwitchNumber = coreSwitchNumber*2
        self.edgeSwitchNumber = coreSwitchNumber*2
        self.hostNumber = self.edgeSwitchNumber*2

        # initialize topology
        Topo.__init__(self)

        # add hosts and switches
        for i in range(1, self.coreSwitchNumber+1):
            prefix = "100"
            self.coreSwitch.append(self.addSwitch(prefix+str(i)))

        for i in range(1, self.aggrSwitchNumber+1):
            prefix = "200"
            self.aggrSwitch.append(self.addSwitch(prefix+str(i)))

        for i in range(1, self.edgeSwitchNumber+1):
            prefix = "300"
            self.edgeSwitch.append(self.addSwitch(prefix+str(i)))

        for i in range(1, self.hostNumber+1):
            prefix = "400"
            self.host.append(self.addHost(prefix+str(i)))

        # add links
        # link core and aggregation
        for i in range(0, self.aggrSwitchNumber):
            self.addLink(self.coreSwitch[0], self.aggrSwitch[i], bw=1000, loss=2)
            self.addLink(self.coreSwitch[1], self.aggrSwitch[i], bw=1000, loss=2)
        # for i in range(1, self.aggrSwitchNumber, 2):
        #     self.addLink(self.coreSwitch[2], self.aggrSwitch[i], bw=1000, loss=2)
        #     self.addLink(self.coreSwitch[3], self.aggrSwitch[i], bw=1000, loss=2)

        # link aggregation and edge
        for i in range(0, self.aggrSwitchNumber, 2):
            self.addLink(self.aggrSwitch[i], self.edgeSwitch[i], bw=100)
            self.addLink(self.aggrSwitch[i], self.edgeSwitch[i+1], bw=100)
            self.addLink(self.aggrSwitch[i+1], self.edgeSwitch[i], bw=100)
            self.addLink(self.aggrSwitch[i+1], self.edgeSwitch[i+1], bw=100)

        # link edge and host
        for i in range(0, self.edgeSwitchNumber):
            self.addLink(self.edgeSwitch[i], self.host[2*i])
            self.addLink(self.edgeSwitch[i], self.host[2*i+1])

topos = { 'mytopo': (lambda: HalfFatTreeTopo()) }
